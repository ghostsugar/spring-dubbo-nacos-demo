package com.zang.test.dubbo.controller;

import com.alibaba.fastjson.JSONObject;
import com.zang.test.dubbo.constant.GetCheck;
import com.zang.test.dubbo.constant.LoginCheck;
import com.zang.test.dubbo.model.entity.Employ;
import com.zang.test.dubbo.model.vo.EmployVO;
import com.zang.test.dubbo.service.EmployService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Zhang Qiang
 * @date 2019/10/24 16:15
 */
@RestController
@RequestMapping("/employ")
public class EmployController {

    @Resource
    EmployService employService;

    @GetMapping("/get")
    public String getEmploy(@Validated(LoginCheck.class) EmployVO vo){
        Employ employ = employService.getByVO(vo);
        return JSONObject.toJSONString(employ);
    }

    @GetMapping("/eId")
    public String eId(@Validated(GetCheck.class) EmployVO vo){
        Employ employ = employService.getByVO(vo);
        return JSONObject.toJSONString(employ);
    }
}
