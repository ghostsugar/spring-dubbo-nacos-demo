package com.zang.test.dubbo;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Zhang Qiang
 * @Date 2019/7/31 18:25
 */
@EnableDubbo
@SpringBootApplication
public class ConSumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConSumerApplication.class, args);
    }

}
