package com.zang.test.dubbo.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zang.test.dubbo.service.Provider;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Zhang Qiang
 * @Date 2019/7/31 18:33
 */
@RestController
public class WebController {

    @Reference(version = "zang.provider.version", check = false)
    Provider helloProvider;

    @GetMapping(value = "/api/hello")
    @ResponseBody
    public String index(@RequestParam("name") String name){
        return helloProvider.hello(name);
    }

    @GetMapping(value = "/api/testMapList")
    @ResponseBody
    public List<Map<String, String>> testMapList(){
        Map<String, String> map = new HashMap<String, String>();
        map.put("hello", "nacos-dubbo");
        return helloProvider.testMapList(map);
    }


}
