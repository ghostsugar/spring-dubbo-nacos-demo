package com.zang.test.dubbo.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author Zhang Qiang
 * @date 2019/10/23 14:25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@NotBlank
@Builder(toBuilder = true)
public class Employ implements Serializable {
    private static final long serialVersionUID = -5514807222888153429L;
    private Integer eId;
    private String eName;
    private String eNum;
    private String ePhone;
    private String eBack;
}
