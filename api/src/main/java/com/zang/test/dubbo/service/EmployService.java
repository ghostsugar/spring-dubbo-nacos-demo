package com.zang.test.dubbo.service;


import com.zang.test.dubbo.model.entity.Employ;
import com.zang.test.dubbo.model.vo.EmployVO;

/**
 * @author Zhang Qiang
 * @date 2019/10/23 14:26
 */
public interface EmployService {

    int insert(Employ employ);

    Employ get(Integer id);

    Employ get(String eName);

    Employ getByVO(EmployVO vo);

}
