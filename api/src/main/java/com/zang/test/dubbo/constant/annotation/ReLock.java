package com.zang.test.dubbo.constant.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * @author Zhang Qiang
 * @date 2019/10/28 11:49
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ReLock {

   /**
    * key
    */
    String key() default "";

    /**
     * true  tryLock(waitTime, leaseTime, unit);
     * false tryLock();
     */
    boolean isHold() default true;

    /**
     * 尝试加锁等待超时时间
     */
    long waitTime() default 50 * 1000L;

    /**
     * 最长持锁时间，到期自动解锁
     */
    long leaseTime() default 50 * 1000L;

    /**
     * 时间格式，默认ms
     */
    TimeUnit timeUnit() default TimeUnit.MICROSECONDS;

}
