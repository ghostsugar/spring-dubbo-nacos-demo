package com.zang.test.dubbo.model.vo;

import com.zang.test.dubbo.constant.GetCheck;
import com.zang.test.dubbo.constant.LoginCheck;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author Zhang Qiang
 * @date 2019/10/24 16:08
 */
@Data
public class EmployVO implements Serializable {

    private static final long serialVersionUID = -1194027982596300325L;

    @NotNull(groups = {LoginCheck.class, GetCheck.class})
    private Integer eId;
    @NotBlank(groups = LoginCheck.class)
    private String eName;
    @NotEmpty(groups = LoginCheck.class)
    private String ePhone;

}
