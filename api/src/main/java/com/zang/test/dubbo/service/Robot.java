package com.zang.test.dubbo.service;

import com.alibaba.dubbo.common.extension.SPI;

/**
 * @author Zhang Qiang
 * @date 2019/10/25 9:47
 */
@SPI
public interface Robot {

    void ai();
}
