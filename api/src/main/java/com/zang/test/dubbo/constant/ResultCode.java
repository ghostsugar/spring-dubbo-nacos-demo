package com.zang.test.dubbo.constant;

/**
 * @author Zhang Qiang
 * @date 2019/10/24 16:38
 */
public enum ResultCode {

    /**
     *
     */
    RESULT_SUCCESS(100000,"success"),
    RESULT_UNKNOWN_EXCEPTION(999999,"未知异常"),
    RESULT_VALIDATION_EXCEPTION(900001,"数据校验异常"),
    RESULT_SERIALIZABLE_NULL(900002,"序列化后是空对像"),
    RESULT_SYSTEM_EXCEPTION(900003,"系统错误"),
    RESULT_SERIALIZABLE_EXCEPTION(900004,"序列化异常"),
    RESULT_NETWORK_EXCEPTION(900005,"网络好像出了点状况哦！"),
    RESULT_NOTHING_EXCEPTION(900006,"什么事情都没有发生"),

    /**
     *
     */
    RESULT_OAUTH2_EXCEPTION(900008,"服务器异常"),
    RESULT_TOKEN_EXCEPTION(900009,"token值为空"),
    RESULT_LOGIN_LOSE(900010,"登录失效"),
    RESULT_SERVICE_EXCEPTION(900011,"服务异常"),
    RESULT_NO_EXISTS_PAGE(900012,"不存在当前页"),
    RESULT_INVALID_TOKEN(900019,"无效的token"),
    RESULT_OAUTH2_CLUELESS(900014,"访问此资源需要完全的身份验证"),
    RESULT_OAUTH2_PERMISSION_DENIED(900015,"权限不足"),
    RESULT_ACCESSCODE_EXCEPTION(900016,"access code 获取异常"),
    RESULT_ACCESS_TOKEN_EXPIRATION(900013,"accessToken 过期"),
    RESULT_REFRESH_TOKEN_EXPIRATION(900018,"refreshToken 过期"),
    CODE_ERROR(900017,"验证码错误"),
    RESULT_DATABASE_ERROR(900020,"数据库错误"),
    RESULT_DATA_ERROR(900021,"数据异常"),
    RESULT_UPDATE_STATUS_ERROR(900022,"修改状态异常"),
    RESULT_UPDATE_PWD_ERROR(900023,"修改密码异常"),

    /**
     * 业务逻辑
     */
    LOGIN_NONEXISTENT(100001,"登录服务异常"),
    LOGIN_USER_NONE(100002,"用户不存在或被禁用"),
    LOGIN_PASSWORD_ERROR(100003,"用户密码错误"),
    LOGIN_USER_STATUS_STOP(100004,"账号已被禁用"),
    LOGIN_USER_DELETED(100005,"账号已被删除"),

    RESULT_USER_EXISTED(100104,"用户已存在"),
    RESULT_USERNAME_EXISTED(100105,"账号已存在"),
    RESULT_PHONE_EXISTED(100106,"手机号已存在"),
    RESULT_USERNAME_LENGTH_MIN(100107,"用户名长度不得低于6位"),
    RESULT_PASSWORD_LENGTH_MIN(100108,"密码长度低于6位或格式不正确"),
    RESULT_DEPT_NULL_LENGTH_MIN(100109,"请选择二级代理"),
    RESULT_DELETE_NOTNULL(100110,"下级代理不为空，请删除所有下级代理"),
    RESULT_INSERT_ERROR(100111,"新增错误"),
    RESULT_BAN_INSERT_DEPT(100112,"禁止此操作"),
    RESULT_NOTHING_UPDATE(100113,"数据未更改"),
    RESULT_COMPANY_CODE_EXISTED(100114,"账号已使用，请换个账号"),

    QUERY_NONE_EXISTS(100201,"未查询到对象"),
    QUERY_NONE_USER_EXISTS(100202,"未查询到用户"),
    QUERY_DELETE_USER_EXISTS(100203,"用户已删除"),
    QUERY_NONE_DEPT_EXISTS(100204,"未查询到部门"),
    QUERY_NONE_ROLE_EXISTS(100205,"未查询到角色"),
    QUERY_NONE_COMPANY_EXISTS(100206,"未查询到公司"),
    QUERY_NONE_DATA_EXISTS(100207,"未查询到数据"),
    QUERY_COMPANY_EXISTS(100208,"公司已存在"),
    QUERY_SALES_DEPT_ID_NULL(100209,"未查询到上级代理或此上级代理已删除"),
    QUERY_DATA_EXISTS(100210,"数据已存在"),

    RESULT_UPDATE_ERROR(100025,"更新异常"),
    UPLOAD_FILE_ERROR(100026," 上传文件错误"),
    UPLOAD_FILE_NONE_EXISTS(100027," 上传文件丢失"),
    CACULATE_ACCOUNT_ERROR(100028," 计算金额不正确"),
    CONFIRM_PASSWORD_ERROR(100029," 两次密码不一致"),
    INVITE_CODE_INVALID(100030," 邀请码无效"),
    PHONE_UNREGISTERED(100031," 手机号未注册"),
    PHONE_SEND_SMS_EXCEPTION(100050," 验证码发送异常，请稍后重试"),
    PHONE_SEND_SMS_INTERVAL(100051," 验证码发送间隔时间60s"),
    SALESMAN_DATA_NULL(100052," 业务员数据为空"),
    NOT_SUFFICIENT_FUNDS (100053,"帐号余额不足"),
    COMPNAY_INEXISTENCE (100054,"公司不存在"),
    CODE_EMPTY_EXCEPTION(100055,"验证码错误"),
    GRAPH_CODE_INVALID (100056,"验证码无效"),
    PARAM_IS_ZERO(100057,"参数为0"),
    PARAM_NO_EXISTS(100058,"参数不存在"),
    PARAM_NULL_EXISTS(100059,"参数为空"),
    PARAM_REAL_NAME_EXISTS(100060,"代理人名称不能为空"),
    PARAM_SALES_USER_NULL_EXISTS(100061,"未查询到代理人相关信息"),

    //定时任务
    SCHEDULED_TASK_EXCEPTION(1000701, "执行定时任务异常"),

    //discuz论坛
    DISCUZ_REGISTR_SUCCESS(100020,"论坛账号添加成功"),
    DISCUZ_REGISTR_ERR(100021,"论坛账号添加失败"),
    DISCUZ_USER_UPDATE_SUCCESS(100040,"论坛账号修改成功"),
    DISCUZ_USER_UPDATE_ERR(100041,"论坛账号修改失败"),
    DISCUZ_LOGIN_SUCEESS(100030,"论坛登录成功"),
    DISCUZ_LOGIN_FAILED(100031,"论坛登录失败"),

    //模板素材
    TEMPLATE_IS_EXIST(100062,"模板已存在"),
    TEMPLATE_CONTAIN_MATERIAL(100063,"模板下包含验证语, 请先清空验证语"),
    UPLOAD_FILE_FAILE(100400,"上传素材失败！请重试"),

    //微信相关
    WECHAT_DATA_PARK_FAIL(200001, "数据包异常"),

    //登录
    WECHAT_QRCODE_GET_FAIL(210001, "获取二维码失败,一分钟后重试"),
    WECHAT_QRCODE_SCAN_TIMEOUT(210002, "超时未扫码"),
    WECHAT_QRCODE_LOGIN_FAIL(210003, "登录失败, 请重新获登录二维码"),
    WECHAT_LOGOUT_FAIL(210004, "微信号退出登录失败"),

    //发送消息
    WECHAT_SEND_MESSAGE_FAIL(220001, "消息发送失败"),
    RECEIVER_IS_EMPTY(220002, "接收者不能为空"),
    CREATE_TASK_FAIL(220003, "创建任务失败"),
    TASK_ID_EMPTY(220003, "任务ID不能为空"),

    //朋友圈
    WECHAT_SEND_SNS_FAIL(230001, "发送朋友圈失败"),

    //联系人
    WECHAT_INIT_CONTACT_FAIL(240001, "初始化联系人失败"),
    WECHAT_GET_CONTACT_FAIL(240002, "获取联系人失败"),

    //redis
    WECHAT_REDIS_KEY_NO_EXISTS(250001, "redis key不存在"),

    //oss
    OSS_UPLOAD_FAIL(260001, "上传文件失败，请检查配置信息"),
    OSS_GET_PROPERTIES_FAIL(260002, "获取文件属性失败"),
    OSS_DOWNLOAD_FAIL(260002, "通过url下载文件");

    private Integer type;
    private String desc;

    ResultCode(Integer type, String desc){
        this.type = type;
        this.desc = desc;
    }
    ResultCode(){}

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static ResultCode fromValue(int v) {
        for (ResultCode t : values()) {
            if (t != null && t.getType() == v) {
                return t;
            }
        }
        return null;
    }
}
