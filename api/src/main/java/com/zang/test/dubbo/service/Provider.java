package com.zang.test.dubbo.service;

import java.util.List;
import java.util.Map;

/**
 * @author Zhang Qiang
 * @Date 2019/7/31 18:20
 */
public interface Provider {


    String hello(String name);

    List<Map<String, String>> testMapList(Map<String, String> map);

}
