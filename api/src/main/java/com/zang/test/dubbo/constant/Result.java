package com.zang.test.dubbo.constant;


import java.io.Serializable;

/**
 * 返回结果
 *
 * @author liyongbin
 * @date 2018-06-06 11:23
 */
public class Result<T> implements Serializable {

    private static final long serialVersionUID = -1400792539705267797L;

    /**
     * 编码
     */
    private Integer code;

    /**
     * 返回消息
     */
    private String msg;

    /**
     * 返回数据
     */
    private T data;


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Result(){}

    public Result(Integer code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public Result(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
    public Result success(){
        return new Result<>(ResultCode.RESULT_SUCCESS.getType(),ResultCode.RESULT_SUCCESS.getDesc());
    }
    public Result success( T data){
        return new Result<>(ResultCode.RESULT_SUCCESS.getType(),ResultCode.RESULT_SUCCESS.getDesc(),data);
    }
    public Result failed(Integer code, String msg){
        return new Result<>(code, msg,null);
    }
}
