package com.zang.test.cache;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class CacheApplicationTests {

    public static final String MESSAGE_KEY = "message:queue";

    @Resource
    RedisClient redisClient;

    @Test
    public void stringRedisClientTest(){
        String k = "ops";
        for (int i = 0; i < 5; i++) {
            redisClient.lpush(k, " msg " + i);
        }
        log.info( k+" size: " + redisClient.llen(k));

        while (true) {
            String v = redisClient.lpop(k);
            if ( v == null ) break;
            log.info("====================");
            log.info( k+" size: " + redisClient.llen(k));
            log.info( "v :" + v);
        }

    }

    @Test
    public void redisClientTest() {
        redisClient.set("90","8899");

        log.info("=============");
        log.info(redisClient.get("90"));














    }





}
