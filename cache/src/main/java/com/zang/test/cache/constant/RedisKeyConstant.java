package com.zang.test.cache.constant;

public class RedisKeyConstant {
   public static final String VOICE_TO_TEXT = "VOICETOTEXT:";
   public static final String COMPANY_KEY = "companyCode";
   public static final String REDIS_TASK_JOB = "RedisTaskJob";
   public static final String REDIS_TASK_LOCK = "RedisTaskLock:";
   public static final long REDIS_MESSAGE_STORE_TIME_THIRD_DAY = 259200000L;
   public static final long REDIS_MESSAGE_STORE_TIME_ONE_DAY = 86400000L;
   public static final String REDIS_MESSAGE_RECORD_KEY = "*:messageRecord:*";
   public static final String REDIS_MESSAGE_KEY = "*:message:*";
   public static final String SESSION_POSFIX = "_sessionId:";
   public static final String COMPANY_POSFIX = "_company:";
   public static final String COMPANY_INFO_POSFIX = "_companyInfo:";
   public static final String WECHAT_NUM_POSFIX = ":wechatNum";
   public static final String WECHAT_RECEIVER_SERVICE = ":wechatReceiverService";
   public static final String WECHAT_USER_POSFIX = "_wechatUser:";
   public static final String KEYWORD_POSFIX = "_keyWord:";
   public static final String CONTACT_POSFIX = "_contact:";
   public static final String UIN_POSFIX = "_uin:";
   public static final String UUID_POSFIX = "_uuid:";
   public static final String CHATROOM_CONTACT_POSFIX = "_chatRoom:contact:";
   public static final String WEBSOCKET_CLIENTID = "_websocket:clientId:";
   public static final String WECHAT_CONTACT = ":wechatContact:";
   public static final String WECHAT_USER_USERINFO = ":wechatMeta:userInfo";
   public static final String WECHAT_USER_WECHATMETA = ":wechatMeta:wechatMeta";
   public static final String SHARE = "share:";
   public static final String MESSAGE_RECORD = ":messageRecord:*";
   public static final String ASSISTANCE_WORK = "AssistanceWord:";
   public static final String REDIS_COLON = ":";
   public static final String WECHAT_NUM = "wechatNum";
}
