package com.zang.test.cache;

import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * redisson 分布式锁
 * @author Zhang Qiang
 * @date 2019/10/28 14:11
 */
@Slf4j
@Component
public class RedissonLockHandler {

    @Autowired
    private RedissonClient redissonClient;

    /**
     * 尝试加锁等待超时时间
     */
    private final static long LOCK_TRY_WAIT = 5 * 1000L;
    /**
     * 持锁时间，防止死锁
     */
    private final static long LOCK_EXPIRE = 10 * 1000L;

    public RLock getRLock(String key){
        return redissonClient.getLock(key);
    }

    public boolean tryLock0(String key) {
        RLock rLock = redissonClient.getLock(key);
        return rLock.tryLock();
    }

    public boolean tryLock(String key) throws InterruptedException {
        return tryLock(key, LOCK_TRY_WAIT, LOCK_EXPIRE);
    }

    public boolean tryLock(String key, long waitTime, long leaseTime) throws InterruptedException {
        return tryLock(key, LOCK_TRY_WAIT, LOCK_EXPIRE, TimeUnit.MILLISECONDS);
    }

    /**
    * @param key 锁
    * @param waitTime 等待时间
    * @param leaseTime 持有锁时间
    * @param unit 单位
    * @return boolean
    */
    public boolean tryLock(String key, long waitTime, long leaseTime, TimeUnit unit) throws InterruptedException {
        RLock rLock = redissonClient.getLock(key);
        return rLock.tryLock(waitTime, leaseTime, unit);
    }

    public void unLock(String key) {
        RLock rLock = redissonClient.getLock(key);
        unLock(rLock);
    }

    public void unLock(RLock rLock){
        if (rLock != null && rLock.isLocked()){
            rLock.unlock();
        }
    }

}
