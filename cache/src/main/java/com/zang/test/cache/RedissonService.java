package com.zang.test.cache;

import org.redisson.api.*;
import org.redisson.config.Config;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * redisson操作类
 */
@Component("redissonService")
public class RedissonService {

    @Resource
    private RedissonClient redissonClient;

    public Config getRedissonClient() throws IOException {
        return redissonClient.getConfig();
    }

    /**`
     * 获取字符串对象
     *
     * @param objectName 对象名称
     * @return bucket
     */
    public <T> RBucket<T> getRBucket(String objectName) {
        return redissonClient.getBucket(objectName);
    }

    /**
     * 获取Map对象
     *
     * @param objectName 对象名称
     * @return bucket
     */
    public <K, V> RMap<K, V> getRMap(String objectName) {
        return redissonClient.getMap(objectName);
    }

    /**
     * 获取有序集合
     *
     * @param objectName 对象名称
     * @return bucket
     */
    public <V> RSortedSet<V> getRSortedSet(String objectName) {
        return redissonClient.getSortedSet(objectName);
    }

    /**
     * 获取集合
     *
     * @param objectName 对象名称
     * @return bucket
     */
    public <V> RSet<V> getRSet(String objectName) {
        return redissonClient.getSet(objectName);
    }

    /**
     * 获取列表
     *
     * @param objectName 对象名称
     * @return bucket
     */
    public <V> RList<V> getRList(String objectName) {
        return redissonClient.getList(objectName);
    }

    /**
     * 获取队列
     *
     * @param objectName 对象名称
     * @return bucket
     */
    public <V> RQueue<V> getRQueue(String objectName) {
        return redissonClient.getQueue(objectName);
    }

    /**
     * 获取双端队列
     *
     * @param objectName 对象名称
     * @return bucket
     */
    public <V> RDeque<V> getRDeque(String objectName) {
        return redissonClient.getDeque(objectName);
    }

    /**
     * 获取锁
     *
     * @param objectName 对象名称
     * @return rLock
     */
    public RLock getRLock(String objectName) {
        return redissonClient.getLock(objectName);
    }

    /**
     * 获取读取锁
     *
     * @param objectName 对象名称
     * @return bucket
     */
    public RReadWriteLock getRWLock(String objectName) {
        return redissonClient.getReadWriteLock(objectName);
    }

    /**
     * 获取原子数
     *
     * @param objectName 对象名称
     * @return bucket
     */
    public RAtomicLong getRAtomicLong(String objectName) {
        return redissonClient.getAtomicLong(objectName);
    }

    /**
     * 获取记数锁
     *
     * @param objectName 对象名称
     * @return bucket
     */
    public RCountDownLatch getRCountDownLatch(String objectName) {
        return redissonClient.getCountDownLatch(objectName);
    }

    /**
     * 获取消息的Topic
     *
     * @param objectName 对象名称
     * @return bucket
     */
    public <T> RTopic getRTopic(String objectName) {
        return redissonClient.getTopic(objectName);
    }
}