package com.zang.test.cache.constant;

/**
 * @author Zhang Qiang
 * @date 2019/10/26 16:17
 */
public class ConfigConstant {
    public static String IS_PRIVATE_YES = "true";
    public static String IS_PRIVATE_NO = "false";
    public static String OPEN_WEBSOCKET_MODULES_YES = "true";
    public static String OPEN_WEBSOCKET_MODULES_NO = "false";
    public static String OPEN_GET_WX_LIMIT_JOB_YES = "true";
    public static String OPEN_GET_WX_LIMIT_JOB_NO = "false";
    public static String OPEN_REDIS_CLUSTER_YES = "true";
    public static String OPEN_REDIS_CLUSTER_NO = "false";
    public static String OPEN_JOB_MODULES_YES = "true";
    public static String OPEN_JOB_MODULES_NO = "false";
    public static String WEBSOCKET_PUSH_MODE_OLD = "0";
    public static String WEBSOCKET_PUSH_MODE_ONLY_STATUS = "1";
}
