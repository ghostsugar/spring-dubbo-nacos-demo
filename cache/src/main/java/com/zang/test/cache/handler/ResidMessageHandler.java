package com.zang.test.cache.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author Zhang Qiang
 * @date 2019/10/28 9:43
 */
@Slf4j
@Component
public class ResidMessageHandler {

    public void receiveMessage(Object msg){
       log.info(" receiveMessage: {}" , msg.toString());
    }

}
