package com.zang.test.cache.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * @author Zhang Qiang
 * @date 2019/10/28 10:04
 */
@Data
@AllArgsConstructor
@Builder(toBuilder = true)
public class RedisLock {
    private String key;
    private String value;
}
