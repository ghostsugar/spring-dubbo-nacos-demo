package com.zang.test.cache;

import com.zang.test.cache.model.RedisLock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * redis 锁
 *
 * @author Zhang Qiang
 * @date 2019/10/28 9:57
 */
@Slf4j
@Component
public class RedisTaskLockHandler {

    @Autowired
    RedisClient redisClient;

    @Autowired
    RedisTemplate redisTemplate;

    /**
     * 是否循环获取锁
     */
    private final static boolean IS_CYCLE = true;

    /**
     * 默认尝试间隔 1000ms
     */
    private final static long LOCK_TRY_INTERVAL = 1000L;

    /**
     * 默认尝试超时时间 10s
     */
    private final static long LOCK_TRY_TIMEOUT = 10 * 1000L;


    /**
     * 单个业务持有锁的时间，防止死锁 30s
     */
    private final static long LOCK_EXPIRE = 30 * 1000L;

    /**
     * 尝试获取全局锁
     *
     * @return
     */
    public boolean tryLock(RedisLock redisLock){
        return getLock(redisLock, LOCK_TRY_TIMEOUT, LOCK_TRY_INTERVAL, LOCK_EXPIRE, IS_CYCLE);
    }
    public boolean tryLock(RedisLock redisLock, long timeOut){
        return getLock(redisLock, timeOut, LOCK_TRY_INTERVAL, LOCK_EXPIRE, IS_CYCLE);
    }
    public boolean tryLock(RedisLock redisLock, long timeOut, long tryInterval){
        return getLock(redisLock, timeOut, tryInterval, LOCK_EXPIRE, IS_CYCLE);
    }
    public boolean tryLock(RedisLock redisLock, boolean is_cycle){
        return getLock(redisLock, LOCK_TRY_TIMEOUT, LOCK_TRY_INTERVAL, LOCK_EXPIRE, is_cycle);
    }
    public boolean tryLock(RedisLock redisLock, long timeOut, boolean is_cycle){
        return getLock(redisLock, timeOut, LOCK_TRY_INTERVAL, LOCK_EXPIRE, is_cycle);
    }
    public boolean tryLock(RedisLock redisLock, long timeOut, long tryInterval, boolean is_cycle){
        return getLock(redisLock, timeOut, tryInterval, LOCK_EXPIRE, is_cycle);
    }
    public boolean tryLock(RedisLock redisLock, long timeOut, long tryInterval, long lockExpireTime, boolean isCycle){
        return getLock(redisLock, timeOut, tryInterval, lockExpireTime, isCycle);
    }

    /**
     * 获取锁/写入锁
     *
     * @param redisLock 锁
     * @param timeOut 超时时间 ms
     * @param tryInterval 尝试间隔 ms
     * @param lockExpireTime 锁过期时间
     * @return boolean
     */
    public boolean getLock( RedisLock redisLock, long timeOut, long tryInterval, long lockExpireTime, boolean isCycle){
        String key = redisLock.getKey();
        try {
            if ( key != null && key.length() != 0){
                if (isCycle){
                    long startTime = System.currentTimeMillis();
                    while (redisTemplate.hasKey(key)){
                        log.warn(" redisLock : {} is exist", key);
                        if ((System.currentTimeMillis() - startTime) > timeOut) {
                            log.warn(" 获取 redisLock 超时 ");
                            return false;
                        }
                        Thread.sleep(tryInterval);
                    }
                }
                if (!redisTemplate.hasKey(key)){
                    setRedisLock(redisLock, lockExpireTime);
                    return true;
                } else {
                    log.warn(" redisLock : {} is exist", key);
                    return false;
                }
            } else {
                throw new IllegalStateException(" redisLock key 不能为空！");
            }
        } catch (InterruptedException e) {
            log.error(" 获取 redisLock 线程等待中断,  {} ", e.getMessage());
            return false;
        }
    }

    /**
     * 设置值
     *
     * @param redisLock
     * @param lockExpireTime
     * @return
     */
    private void setRedisLock(RedisLock redisLock, long lockExpireTime ){
        ValueOperations operations = redisTemplate.opsForValue();
        operations.set(redisLock.getKey(), redisLock.getValue(), lockExpireTime, TimeUnit.MILLISECONDS);
    }

    /**
     * 释放锁
     *
     * @param lock
     * @param value
     * @return
     */
    public void releaseLock(RedisLock lock, String value) {
        this.releaseLock(lock.getKey(), value);
    }
    public void releaseLock(String key, String value) {
        if (key == null && key.length() == 0) {
            log.info(" There is no lock : {} ", key);
        } else {
            ValueOperations operations = redisTemplate.opsForValue();
            String v = (String) operations.get(key);
            if (!v.equals(value)){
                throw new IllegalStateException(" === 非正常释放锁 === ");
            } else {
                redisTemplate.delete(key);
            }
        }
    }





}
