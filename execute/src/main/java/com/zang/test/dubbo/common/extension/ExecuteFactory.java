package com.zang.test.dubbo.common.extension;

import com.alibaba.dubbo.common.extension.SPI;

/**
 * @author Zhang Qiang
 * @date 2019/10/25 11:47
 */
@SPI
public interface ExecuteFactory {
    <T> T getExecute(Class<T> v, String s);
}
