package com.zang.test.dubbo;

import com.alibaba.dubbo.common.extension.ExtensionLoader;
import com.zang.test.dubbo.service.Robot;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Enumeration;
import java.util.ServiceLoader;

@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class ProviderApplicationTest {

    @Test
    public void javaSPITest(){
        ServiceLoader<Robot> serviceLoader = ServiceLoader.load(Robot.class);
        System.out.println(" ================= JAVA SPI ================= ");
        serviceLoader.forEach(Robot::ai);
        System.out.println(" ================= JAVA SPI ================= ");
    }


    @Test
    public void dubboSPITest(){
        ExtensionLoader<Robot> extensionLoader = ExtensionLoader.getExtensionLoader(Robot.class);
        Robot o = extensionLoader.getExtension("optimusPrimeRobot");
        Robot b = extensionLoader.getExtension("blueBeeRobot");

        o.ai();
        b.ai();
    }

    @Test
    public void loadDirectory() throws IOException {
        String fileName = "META-INF/dubbo/com.zang.test.dubbo.service.Robot";
        ClassLoader classLoader = this.getClass().getClassLoader();
        Enumeration enumeration = null;
        if (classLoader == null){
            enumeration = ClassLoader.getSystemResources(fileName);
        } else {
            enumeration = classLoader.getResources(fileName);
        }

        if (enumeration != null) {
            while (enumeration.hasMoreElements()){
                java.net.URL resourceURL = (URL) enumeration.nextElement();
                log.info("=====================================");
                log.info(resourceURL.toString());
                BufferedReader br = new BufferedReader(new InputStreamReader(resourceURL.openStream()));
                try {
                    String line;
                    while ((line = br.readLine())!= null){
                        //截取注释之前字符串
                        final int ci = line.indexOf("#");
                        if (ci >= 0){
                            line = line.substring(0, ci).trim();
                        }
                        //加载并保存缓存
                        if (line.length()>0){
                            String name = null;
                            //读取配置文件
                            int i = line.indexOf("=");
                            if (i > 0){
                                name = line.substring(0, i).trim();
                                line = line.substring(i+1).trim();
                            }
                            log.info(line);
                        }
                    }
                } finally {
                    if (br!=null) br.close();
                }

            }
        }
    }





}
