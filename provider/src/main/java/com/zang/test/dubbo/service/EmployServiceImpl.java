package com.zang.test.dubbo.service;

import com.zang.test.dubbo.model.entity.Employ;
import com.zang.test.dubbo.model.vo.EmployVO;
import com.alibaba.dubbo.config.annotation.Service;

/**
 * @author Zhang Qiang
 * @date 2019/10/23 14:27
 */
@Service(version = "zang.provider.version")
public class EmployServiceImpl implements EmployService {

    @Override
    public int insert(Employ employ) {
        return 1;
    }

    @Override
    public Employ get(Integer id) {
        return Employ.builder().eId(id).eName("eWord").eNum("559").ePhone("0998786").build();
    }

    @Override
    public Employ get(String eName) {
        if (eName !=null && eName.length() != 0){
            return Employ.builder().eId(99).eName(eName).eNum("8899").ePhone("13858").build();
        } else {
            return new Employ();
        }
    }

    @Override
    public Employ getByVO(EmployVO vo) {
        return Employ.builder().eId(vo.getEId()).eName(vo.getEName()).eNum("???").ePhone(vo.getEPhone()).build();
    }
}
