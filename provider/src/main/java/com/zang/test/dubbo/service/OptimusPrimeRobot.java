package com.zang.test.dubbo.service;


import lombok.extern.slf4j.Slf4j;

/**
 * @author Zhang Qiang
 * @date 2019/10/25 9:47
 */
@Slf4j
public class OptimusPrimeRobot implements Robot{
    @Override
    public void ai() {
        log.info("aiaiaiaiai");
    }
}
