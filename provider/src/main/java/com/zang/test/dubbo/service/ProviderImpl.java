package com.zang.test.dubbo.service;

import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Zhang Qiang
 * @Date 2019/7/31 18:24
 */
@Service(version = "zang.provider.version")
public class ProviderImpl implements Provider {

    @Value("${spring.profiles.active}")
    private String env;

    @Override
    public String hello(String name) {
        return "hello, " + name;
    }

    @Override
    public List<Map<String, String>> testMapList(Map<String, String> map) {
        List<Map<String, String>> list = new ArrayList<Map<String, String>>();
        list.add(map);
        Map<String, String> map2 = new HashMap<String, String>();
        map2.put("env", env);
        map2.put("testKey", "testValue");
        list.add(map2);
        return list;
    }




}

