package com.zang.test.rocketmq.constant;

/**
 * @author Zhang Qiang
 * @date 2019/10/31 17:55
 */
public class RocketKeyContant {
    public static final String SYNC_TOPIC = "sync_topic";
}
