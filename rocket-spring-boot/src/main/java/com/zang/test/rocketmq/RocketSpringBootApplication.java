package com.zang.test.rocketmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Zhang Qiang
 * @date 2019/10/31 17:43
 */
@SpringBootApplication
public class RocketSpringBootApplication {
    public static void main(String[] args) {
        SpringApplication.run(RocketSpringBootApplication.class, args);
    }

}
