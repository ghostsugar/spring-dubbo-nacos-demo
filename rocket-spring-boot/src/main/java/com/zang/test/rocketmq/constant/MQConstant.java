package com.zang.test.rocketmq.constant;

/**
 * @author Zhang Qiang
 * @date 2019/10/31 15:27
 */
public class MQConstant {

    public static final String QUEUE_MESSAGE_KEY = "queue_message_topic";
    public static final String QUEUE_RESPONSE_TOPIC = "queue_response_topic";


    public static final String CHARSET_UTF_8 = "UTF-8";


}
