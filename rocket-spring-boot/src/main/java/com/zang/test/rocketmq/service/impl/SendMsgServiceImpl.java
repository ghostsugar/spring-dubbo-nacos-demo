package com.zang.test.rocketmq.service.impl;

import com.zang.test.rocketmq.constant.RocketKeyContant;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Zhang Qiang
 * @date 2019/10/31 17:46
 */
@Service
public class SendMsgServiceImpl<T> {

    @Resource
    private RocketMQTemplate rocketMqTemplate;

    @Value("${rocketmq.producer.group}")
    private String group;

    public Map<String, ? extends T> convertAndSend(String msg){
        rocketMqTemplate.convertAndSend(RocketKeyContant.SYNC_TOPIC, msg);
        return new HashMap<>();
    }

}
