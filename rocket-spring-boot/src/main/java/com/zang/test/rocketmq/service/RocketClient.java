package com.zang.test.rocketmq.service;

import com.zang.test.rocketmq.constant.MQConstant;
import com.zang.test.rocketmq.model.MsgEvt;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;

/**
 * 监听消息
 *
 * @author Zhang Qiang
 * @date 2019/10/31 14:35
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = MQConstant.QUEUE_RESPONSE_TOPIC, consumerGroup = "PushConsumer")
public class RocketClient implements RocketMQListener<MsgEvt> {

    @Override
    public void onMessage(MsgEvt s) {
        this.processMessage(s);
    }

    private void processMessage(MsgEvt s){
        log.info(" 收到消息 <==== : {}", new String(s.getMessage().getBody(), StandardCharsets.UTF_8));
    }


}
