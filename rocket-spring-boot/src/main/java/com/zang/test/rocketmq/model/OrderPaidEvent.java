package com.zang.test.rocketmq.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Zhang Qiang
 * @date 2019/10/31 16:17
 */
@Data
@AllArgsConstructor
public class OrderPaidEvent {
    private String orderId;
    private BigDecimal paidMoney;
}
