package com.zang.test.rocketmq.mq.consumer;

import com.zang.test.rocketmq.model.QueueDateMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author Zhang Qiang
 * @date 2019/11/19 16:25
 */
@Slf4j
@Component
@RocketMQMessageListener(topic = "Boot-boot-boot", consumerGroup = "Boot-boot-boot")
public class BootConsumer implements RocketMQListener<QueueDateMessage> {

    @Override
    public void onMessage(QueueDateMessage msg) {
        log.info("收到消息");
        this.proTest(msg);
    }

    private void proTest(QueueDateMessage<Map<String, String>> msg){
        Map<String, String> map = msg.getDate();
        map.forEach((String k, String v) -> {
            log.info("{}:{}",k,v);
        });

    }

}
