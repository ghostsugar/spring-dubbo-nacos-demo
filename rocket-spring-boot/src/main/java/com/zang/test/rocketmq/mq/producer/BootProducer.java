package com.zang.test.rocketmq.mq.producer;

import com.zang.test.rocketmq.model.QueueDateMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author Zhang Qiang
 * @date 2019/11/19 16:28
 */
@Slf4j
@Component
public class BootProducer<T> {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    public void sendMqTest(QueueDateMessage<Map<String, Integer>> msg){
        log.info("发送mq======");
        log.info("rocketMQTemplate.getProducer() : {}", rocketMQTemplate.getProducer());
        rocketMQTemplate.asyncSend("Boot-boot-boot", msg, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                log.info("发送成功 ");
            }
            @Override
            public void onException(Throwable throwable) {
                log.error("发送失败 msg:{}", throwable.getMessage());
            }
        });
    }
}
