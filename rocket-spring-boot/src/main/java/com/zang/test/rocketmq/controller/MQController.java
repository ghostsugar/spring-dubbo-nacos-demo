package com.zang.test.rocketmq.controller;

import com.zang.test.rocketmq.model.QueueDateMessage;
import com.zang.test.rocketmq.mq.producer.BootProducer;
import com.zang.test.rocketmq.service.SendMqMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Zhang Qiang
 * @date 2019/10/31 14:47
 */
@Slf4j
@RestController
public class MQController {

    @Autowired
    private SendMqMessageService sendMqMessageService;

    @Autowired
    private BootProducer bootProducer;

    @PostMapping("/send")
    public Object sendMq(@RequestBody String str) throws UnsupportedEncodingException {
        return sendMqMessageService.sendMessage(str);
    }

    @GetMapping("/sendmq/{str}")
    public void sendmq(@PathVariable("str")String str){
        QueueDateMessage<Map<String, String>> msg = new QueueDateMessage();
        Map<String, String> map = new HashMap<>();
        map.put("ato", "2");
        map.put("company", "com56");
        map.put("banana", "apple");
        map.put("str", str);
        msg.setDate(map);
        bootProducer.sendMqTest(msg);
    }

}
