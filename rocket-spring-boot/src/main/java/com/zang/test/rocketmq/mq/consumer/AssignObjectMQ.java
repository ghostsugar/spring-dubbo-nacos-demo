package com.zang.test.rocketmq.mq.consumer;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 * @author Zhang Qiang
 * @date 2019/11/20 10:40
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = "AssignProductUser", consumerGroup = "AssignProductUser")
public class AssignObjectMQ implements RocketMQListener<String> {
    @Override
    public void onMessage(String o) {
        log.info(" AssignProductUser 收到消息 {} ", o);

    }
}
