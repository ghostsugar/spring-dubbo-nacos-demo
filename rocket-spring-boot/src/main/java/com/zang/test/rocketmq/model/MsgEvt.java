package com.zang.test.rocketmq.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.rocketmq.common.message.Message;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Zhang Qiang
 * @date 2019/10/31 16:02
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class MsgEvt implements Serializable {
    private static final long serialVersionUID = -6082308353026361724L;
    private String orderId;
    private BigDecimal paidMoney;
    private Message message;
}
